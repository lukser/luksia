#include <Luksia/Network/Socket.hpp>
#include <Luksia/Network/SocketImpl.hpp>
#include <Luksia/System/Err.hpp>

namespace lk {
    Socket::Socket(Type type) : m_type(type), m_socket(priv::SocketImpl::invalidSocket()), m_isBlocking(true) {}

    Socket::~Socket() {
        close();
    }

    void Socket::setBlocking(bool blocking) {
        if (m_socket != priv::SocketImpl::invalidSocket())
            priv::SocketImpl::setBlocking(m_socket, blocking);

        m_isBlocking = blocking;
    }

    bool Socket::isBlocking() const {
        return m_isBlocking;
    }

    SocketHandle Socket::getHandle() const {
        return m_socket;
    }

    void Socket::create() {
        if (m_socket == priv::SocketImpl::invalidSocket()) {
            SocketHandle handle = socket(PF_INET, m_type == Tcp ? SOCK_STREAM : SOCK_DGRAM, 0);

            if (handle == priv::SocketImpl::invalidSocket()) {
                err() << "Failed to create socket" << std::endl;
                return;
            }

            create(handle);
        }
    }

    void Socket::create(SocketHandle handle) {
        if (m_socket != priv::SocketImpl::invalidSocket()) return;
        m_socket = handle;
        setBlocking(m_isBlocking);

        if (m_type == Tcp) {
            int yes = 1;
            if (setsockopt(m_socket, IPPROTO_TCP, TCP_NODELAY, reinterpret_cast<char *>(&yes), sizeof(yes)) == -1) {
                err() << "Failed to set socket option \"TCP_NODELAY\" ; "
                      << "all your TCP packets will be buffered" << std::endl;
            }

#if defined(LUKSIA_SYSTEM_MACOS)
            if (setsockopt(m_socket, SOL_SOCKET, SO_NOSIGPIPE, reinterpret_cast<char*>(&yes), sizeof(yes)) == -1) {
                err() << "Failed to set socket option \"SO_NOSIGPIPE\"" << std::endl;
            }
#endif
        } else {
            int yes = 1;
            if (setsockopt(m_socket, SOL_SOCKET, SO_BROADCAST, reinterpret_cast<char *>(&yes), sizeof(yes)) == -1) {
                err() << "Failed to enable broadcast on UDP socket" << std::endl;
            }
        }
    }

    void Socket::close() {
        if (m_socket != priv::SocketImpl::invalidSocket()) {
            priv::SocketImpl::close(m_socket);
            m_socket = priv::SocketImpl::invalidSocket();
        }
    }
}
