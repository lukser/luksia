#ifndef LUKSIA_NETWORK_SOCKETIMPL_HPP
#define LUKSIA_NETWORK_SOCKETIMPL_HPP

#include <Luksia/Config.hpp>

#if defined(LUKSIA_SYSTEM_WINDOWS)

#include <Luksia/Network/Win32/SocketImpl.hpp>

#else

#include <Luksia/Network/Unix/SocketImpl.hpp>

#endif

#endif
