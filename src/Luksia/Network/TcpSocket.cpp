#include <Luksia/Network/IpAddress.hpp>
#include <Luksia/Network/Packet.hpp>
#include <Luksia/Network/SocketImpl.hpp>
#include <Luksia/Network/TcpSocket.hpp>
#include <Luksia/System/Err.hpp>

#include <memory>


namespace lk {
    TcpSocket::TcpSocket() : Socket(Type::Tcp) {}

    std::uint16_t TcpSocket::getLocalPort() const {
        if (getHandle() == priv::SocketImpl::invalidSocket()) return 0;
        sockaddr_in address{};
        if (priv::SocketImpl::AddrLength size = sizeof(address);
                getsockname(getHandle(), std::bit_cast<sockaddr *>(&address), &size) != -1)
            return ntohs(address.sin_port);
        return 0;
    }

    std::optional<IpAddress> TcpSocket::getRemoteAddress() const {
        if (getHandle() == priv::SocketImpl::invalidSocket()) return std::nullopt;
        sockaddr_in address{};
        if (priv::SocketImpl::AddrLength size = sizeof(address);
                getpeername(getHandle(), std::bit_cast<sockaddr *>(&address), &size) != -1)
            return IpAddress(ntohl(address.sin_addr.s_addr));
        return std::nullopt;
    }

    std::uint16_t TcpSocket::getRemotePort() const {
        if (getHandle() == priv::SocketImpl::invalidSocket()) return 0;
        sockaddr_in address{};
        if (priv::SocketImpl::AddrLength size = sizeof(address);
                getpeername(getHandle(), std::bit_cast<sockaddr *>(&address), &size) != -1)
            return ntohs(address.sin_port);
        return 0;
    }

    Socket::Status TcpSocket::connect(const IpAddress &remoteAddress, std::uint16_t remotePort, Time timeout) {
        disconnect();
        create();

        sockaddr_in address = priv::SocketImpl::createAddress(remoteAddress.toInteger(), remotePort);
        if (timeout <= Time::Zero) {
            if (::connect(getHandle(), std::bit_cast<sockaddr *>(&address), sizeof(address)) == -1)
                return priv::SocketImpl::getErrorStatus();
            return Status::Done;
        }

        bool blocking = isBlocking();

        if (blocking) setBlocking(false);
        if (::connect(getHandle(), std::bit_cast<sockaddr *>(&address), sizeof(address)) == -1) {
            setBlocking(blocking);
            return Status::Done;
        }

        Status status = priv::SocketImpl::getErrorStatus();
        if (!blocking) return status;

        if (status == Socket::Status::NotReady) {
            fd_set selector;
            FD_ZERO(&selector);
            FD_SET(getHandle(), &selector);

            timeval time{};
            time.tv_sec = static_cast<long>(timeout.asMicroseconds() / 1000000);
            time.tv_usec = static_cast<int>(timeout.asMicroseconds() % 1000000);

            if (::select(static_cast<int>(getHandle()), nullptr, &selector, nullptr, &time) > 0) {
                status = getRemoteAddress().has_value() ? Status::Done : priv::SocketImpl::getErrorStatus();
            } else {
                status = priv::SocketImpl::getErrorStatus();
            }
        }

        setBlocking(true);
        return status;
    }
}
