
#ifndef LUKSIA_NETWORK_WIN32_SOCKETIMPL_HPP
#define LUKSIA_NETWORK_WIN32_SOCKETIMPL_HPP

#ifdef _WIN32_WINDOWS
#undef _WIN32_WINDOWS
#endif

#ifdef _WIN32_WINNT
#undef _WIN32_WINNT
#endif

#define _WIN32_WINDOWS 0x0501
#define _WIN32_WINNT 0x0501

#include <Luksia/Network/Socket.hpp>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <cstdint>

namespace lk::priv {
    class SocketImpl {
    public:
        using AddrLength = int;

        static sockaddr_in createAddress(std::uint32_t address, unsigned short port);

        static SocketHandle invalidSocket();

        static void close(SocketHandle sock);

        static void setBlocking(SocketHandle sock, bool block);

        static Socket::Status getErrorStatus();
    };
}

#endif
