#include <Luksia/Network/Packet.hpp>
#include <Luksia/Network/SocketImpl.hpp>
#include <cstring>
#include <cwchar>

namespace lk {
    Packet::Packet() = default;

    Packet::~Packet() = default;

    Packet::Packet(const Packet &other) = default;

    Packet &Packet::operator=(const Packet &other) = default;

    Packet::Packet(Packet &&other) noexcept = default;

    Packet &Packet::operator=(Packet &&other) noexcept = default;

    void Packet::append(const void *data, std::size_t sizeInBytes) {
        if (data && (sizeInBytes > 0)) {
            std::size_t start = m_data.size();
            m_data.resize(start + sizeInBytes);
            std::memcpy(&m_data[start], data, sizeInBytes);
        }
    }

    std::size_t Packet::getReadPosition() const {
        return m_readPosition;
    }

    void Packet::clear() {
        m_data.clear();
        m_readPosition = 0;
        m_isValid = true;
    }

    const void *Packet::getData() const {
        return !m_data.empty() ? m_data.data() : nullptr;
    }

    std::size_t Packet::getDataSize() const {
        return m_data.size();
    }

    bool Packet::endOfPacket() const {
        return m_readPosition == m_data.size();
    }

    Packet::operator bool() const {
        return m_isValid;
    }

    Packet &Packet::operator>>(bool &data) {
        if (std::uint8_t value; *this >> value)
            data = (value != 0);

        return *this;
    }

    Packet &Packet::operator>>(std::int8_t &data) {
        if (checkSize(sizeof(data))) {
            std::memcpy(&data, &m_data[m_readPosition], sizeof(data));
            m_readPosition += sizeof(data);
        }

        return *this;
    }

    Packet &Packet::operator>>(std::uint8_t &data) {
        if (checkSize(sizeof(data))) {
            std::memcpy(&data, &m_data[m_readPosition], sizeof(data));
            m_readPosition += sizeof(data);
        }

        return *this;
    }

    Packet &Packet::operator>>(std::int16_t &data) {
        if (checkSize(sizeof(data))) {
            std::memcpy(&data, &m_data[m_readPosition], sizeof(data));
            data = static_cast<std::int16_t>(ntohs(static_cast<uint16_t>(data)));
            m_readPosition += sizeof(data);
        }

        return *this;
    }

    Packet &Packet::operator>>(std::uint16_t &data) {
        if (checkSize(sizeof(data))) {
            std::memcpy(&data, &m_data[m_readPosition], sizeof(data));
            data = ntohs(data);
            m_readPosition += sizeof(data);
        }

        return *this;
    }

    Packet &Packet::operator>>(std::int32_t &data) {
        if (checkSize(sizeof(data))) {
            std::memcpy(&data, &m_data[m_readPosition], sizeof(data));
            data = static_cast<std::int32_t>(ntohl(static_cast<uint32_t>(data)));
            m_readPosition += sizeof(data);
        }

        return *this;
    }

    Packet &Packet::operator>>(std::uint32_t &data) {
        if (checkSize(sizeof(data))) {
            std::memcpy(&data, &m_data[m_readPosition], sizeof(data));
            data = ntohl(data);
            m_readPosition += sizeof(data);
        }

        return *this;
    }

    Packet &Packet::operator>>(std::int64_t &data) {
        if (checkSize(sizeof(data))) {
            std::uint8_t bytes[sizeof(data)];
            std::memcpy(&bytes, &m_data[m_readPosition], sizeof(data));
            data = (static_cast<int64_t>(bytes[0]) << 56) |
                   (static_cast<int64_t>(bytes[1]) << 48) |
                   (static_cast<int64_t>(bytes[2]) << 40) |
                   (static_cast<int64_t>(bytes[3]) << 32) |
                   (static_cast<int64_t>(bytes[4]) << 24) |
                   (static_cast<int64_t>(bytes[5]) << 16) |
                   (static_cast<int64_t>(bytes[6]) << 8) |
                   (static_cast<int64_t>(bytes[7]));
            m_readPosition += sizeof(data);
        }

        return *this;
    }

    Packet &Packet::operator>>(std::uint64_t &data) {
        if (checkSize(sizeof(data))) {
            std::uint8_t bytes[sizeof(data)];
            std::memcpy(&bytes, &m_data[m_readPosition], sizeof(data));
            data = (static_cast<uint64_t>(bytes[0]) << 56) |
                   (static_cast<uint64_t>(bytes[1]) << 48) |
                   (static_cast<uint64_t>(bytes[2]) << 40) |
                   (static_cast<uint64_t>(bytes[3]) << 32) |
                   (static_cast<uint64_t>(bytes[4]) << 24) |
                   (static_cast<uint64_t>(bytes[5]) << 16) |
                   (static_cast<uint64_t>(bytes[6]) << 8) |
                   (static_cast<uint64_t>(bytes[7]));
            m_readPosition += sizeof(data);
        }

        return *this;
    }

    Packet &Packet::operator>>(float &data) {
        if (checkSize(sizeof(data))) {
            std::memcpy(&data, &m_data[m_readPosition], sizeof(data));
            m_readPosition += sizeof(data);
        }

        return *this;
    }

    Packet &Packet::operator>>(double &data) {
        if (checkSize(sizeof(data))) {
            std::memcpy(&data, &m_data[m_readPosition], sizeof(data));
            m_readPosition += sizeof(data);
        }

        return *this;
    }

    Packet &Packet::operator>>(char *data) {
        std::uint32_t length = 0;
        *this >> length;
        if ((length > 0) && checkSize(length)) {
            std::memcpy(&data, &m_data[m_readPosition], length);
            data[length] = '\0';
            m_readPosition += length;
        }

        return *this;
    }

    Packet &Packet::operator>>(std::string &data) {
        std::uint32_t length = 0;
        *this >> length;
        data.clear();

        if ((length > 0) && checkSize(length)) {
            data.assign(&m_data[m_readPosition], length);
            m_readPosition += length;
        }

        return *this;
    }

    Packet &Packet::operator>>(wchar_t *data) {
        std::uint32_t length = 0;
        *this >> length;
        if ((length > 0) && checkSize(length * sizeof(std::uint32_t))) {
            for (auto i = 0; i < length; ++i) {
                std::uint32_t character = 0;
                *this >> character;
                data[i] = static_cast<wchar_t>(character);
            }
            data[length] = L'\0';
        }

        return *this;
    }

    Packet &Packet::operator>>(std::wstring &data) {
        std::uint32_t length = 0;
        *this >> length;
        data.clear();

        if ((length > 0) && checkSize(length * sizeof(std::uint32_t))) {
            for (auto i = 0; i < length; ++i) {
                std::uint32_t character = 0;
                *this >> character;
                data += static_cast<wchar_t>(character);
            }
        }

        return *this;
    }

    Packet &Packet::operator<<(bool data) {
        *this << static_cast<std::uint8_t>(data);
        return *this;
    }

    Packet &Packet::operator<<(std::int8_t data) {
        this->append(&data, sizeof(data));
        return *this;
    }

    Packet &Packet::operator<<(std::uint8_t data) {
        this->append(&data, sizeof(data));
        return *this;
    }

    Packet &Packet::operator<<(std::int16_t data) {
        auto toWrite = static_cast<std::int16_t>(htons(static_cast<std::uint16_t>(data)));
        this->append(&toWrite, sizeof(toWrite));
        return *this;
    }

    Packet &Packet::operator<<(std::uint16_t data) {
        std::uint16_t toWrite = htons(data);
        this->append(&toWrite, sizeof(data));
        return *this;
    }

    Packet &Packet::operator<<(std::int32_t data) {
        auto toWrite = static_cast<std::int32_t>(htonl(static_cast<std::uint32_t>(data)));
        this->append(&toWrite, sizeof(toWrite));
        return *this;
    }

    Packet &Packet::operator<<(std::uint32_t data) {
        std::uint32_t toWrite = htonl(data);
        this->append(&toWrite, sizeof(data));
        return *this;
    }

    Packet &Packet::operator<<(std::int64_t data) {
        std::uint8_t toWrite[] = {
                static_cast<std::uint8_t>((data >> 56) & 0xFF),
                static_cast<std::uint8_t>((data >> 48) & 0xFF),
                static_cast<std::uint8_t>((data >> 40) & 0xFF),
                static_cast<std::uint8_t>((data >> 32) & 0xFF),
                static_cast<std::uint8_t>((data >> 24) & 0xFF),
                static_cast<std::uint8_t>((data >> 16) & 0xFF),
                static_cast<std::uint8_t>((data >> 8) & 0xFF),
                static_cast<std::uint8_t>((data) & 0xFF),
        };
        this->append(&toWrite, sizeof(toWrite));
        return *this;
    }

    Packet &Packet::operator<<(std::uint64_t data) {
        std::uint8_t toWrite[] = {
                static_cast<std::uint8_t>((data >> 56) & 0xFF),
                static_cast<std::uint8_t>((data >> 48) & 0xFF),
                static_cast<std::uint8_t>((data >> 40) & 0xFF),
                static_cast<std::uint8_t>((data >> 32) & 0xFF),
                static_cast<std::uint8_t>((data >> 24) & 0xFF),
                static_cast<std::uint8_t>((data >> 16) & 0xFF),
                static_cast<std::uint8_t>((data >> 8) & 0xFF),
                static_cast<std::uint8_t>((data) & 0xFF),
        };
        this->append(&toWrite, sizeof(toWrite));
        return *this;
    }

    Packet &Packet::operator<<(float data) {
        this->append(&data, sizeof(data));
        return *this;
    }

    Packet &Packet::operator<<(double data) {
        this->append(&data, sizeof(data));
        return *this;
    }

    Packet &Packet::operator<<(const char *data) {
        auto length = static_cast<std::uint32_t>(std::strlen(data));
        *this << length;
        this->append(data, length * sizeof(char));
        return *this;
    }

    Packet &Packet::operator<<(const std::string &data) {
        auto length = static_cast<std::uint32_t>(data.size());
        *this << length;
        if (length > 0) this->append(data.c_str(), length * sizeof(std::string::value_type));
        return *this;
    }

    Packet &Packet::operator<<(const wchar_t *data) {
        auto length = static_cast<std::uint32_t>(std::wcslen(data));
        *this << length;
        for (auto c = data; *c != L'\0'; ++c)
            *this << static_cast<std::uint32_t>(*c);
        return *this;
    }

    Packet &Packet::operator<<(const std::wstring &data) {
        auto length = static_cast<std::uint32_t>(data.size());
        *this << length;
        if (length > 0) {
            for (auto c: data) *this << static_cast<std::uint32_t>(c);
        }
        return *this;
    }

    bool Packet::checkSize(std::size_t size) {
        m_isValid = m_isValid && (m_readPosition + size <= m_data.size());
        return m_isValid;
    }

    const void *Packet::onSend(std::size_t &size) {
        size = getDataSize();
        return getData();
    }

    void Packet::onReceive(const void *data, std::size_t size) {
        this->append(data, size);
    }
}
