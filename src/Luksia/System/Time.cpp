#include <Luksia/System/Time.hpp>

namespace lk {
    constexpr Time::Time() = default;

    template<typename Rep, typename Period>
    constexpr Time::Time(const std::chrono::duration<Rep, Period> &duration) : m_microseconds(duration) {}

    constexpr float Time::asSeconds() const {
        return std::chrono::duration<float>(m_microseconds).count();
    }

    constexpr std::int32_t Time::asMilliseconds() const {
        return std::chrono::duration_cast<std::chrono::duration<std::int32_t, std::milli>>(m_microseconds).count();
    }

    constexpr std::int64_t Time::asMicroseconds() const {
        return m_microseconds.count();
    }

    constexpr std::chrono::microseconds Time::toDuration() const {
        return m_microseconds;
    }

    template<typename Rep, typename Period>
    constexpr Time::operator std::chrono::duration<Rep, Period>() const {
        return m_microseconds;
    }

    constexpr Time seconds(float amount) {
        return Time(std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::duration<float>(amount)));
    }

    constexpr Time milliseconds(std::int32_t amount) {
        return Time(std::chrono::milliseconds(amount));
    }

    constexpr Time microseconds(std::int64_t amount) {
        return Time(std::chrono::microseconds(amount));
    }

    constexpr Time operator-(Time right) {
        return microseconds(-right.asMicroseconds());
    }

    constexpr Time operator-(Time left, Time right) {
        return microseconds(left.asMicroseconds() - right.asMicroseconds());
    }

    constexpr Time operator-=(Time &left, Time right) {
        return left = left - right;
    }

    constexpr Time operator+(Time left, Time right) {
        return microseconds(left.asMicroseconds() + right.asMicroseconds());
    }

    constexpr Time operator+=(Time &left, Time right) {
        return left = left + right;
    }

    constexpr Time operator*(Time left, float right) {
        return seconds(left.asSeconds() * right);
    }

    constexpr Time operator*(Time left, std::int64_t right) {
        return microseconds(left.asMicroseconds() * right);
    }

    constexpr Time operator*(float left, Time right) {
        return right * left;
    }

    constexpr Time operator*(std::int64_t left, Time right) {
        return right * left;
    }

    constexpr Time &operator*=(Time &left, float right) {
        return left = left * right;
    }

    constexpr Time &operator*=(Time &left, std::int64_t right) {
        return left = left * right;
    }

    constexpr float operator/(Time left, Time right) {
        return left.asSeconds() / right.asSeconds();
    }

    constexpr Time operator/(Time left, float right) {
        return seconds(left.asSeconds() / right);
    }

    constexpr Time operator/(Time left, std::int64_t right) {
        return microseconds(left.asMicroseconds() / right);
    }

    constexpr Time &operator/=(Time &left, float right) {
        return left = left / right;
    }

    constexpr Time &operator/=(Time &left, std::int64_t right) {
        return left = left / right;
    }

    constexpr Time operator%(Time left, Time right) {
        return microseconds(left.asMicroseconds() % right.asMicroseconds());
    }

    constexpr Time &operator%=(Time &left, Time right) {
        return left = left % right;
    }

    constexpr Time Time::Zero;
}
