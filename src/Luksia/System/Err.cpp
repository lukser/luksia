#include <Luksia/System/Err.hpp>

namespace {
    class DefaultErrStreamBuf final : public std::streambuf {
    public:
        DefaultErrStreamBuf() {
            static const int size = 64;
            char *buffer = new char[size];
            setp(buffer, buffer + size);
        }

        ~DefaultErrStreamBuf() final {
            sync();
            delete[] pbase();
        }

    private:
        virtual int overflow(int character) {
            if ((character != EOF) && (pptr() != epptr())) {
                return sputc(static_cast<char>(character));
            } else if (character != EOF) {
                sync();
                return overflow(character);
            } else {
                return sync();
            }
        }

        virtual int sync() {
            if (pbase() != pptr()) {
                std::size_t size = static_cast<int>(pptr() - pbase());
                fwrite(pbase(), 1, size, stderr);

                setp(pbase(), epptr());
            }

            return 0;
        }
    };
}

namespace lk {
    std::ostream &err() {
        static DefaultErrStreamBuf buffer;
        static std::ostream stream(&buffer);

        return stream;
    }
}
