# Luksia

Graphics rendering engine highly influenced by SFML. Uses GL under the hood.

## Getting started

```
$ meson compile -C builddir
```

# Features

- [X] Cross-Platform
- [X] Open-Source
- [ ] Graphics
- [ ] Window Management
- [ ] Audio
- [ ] Networking
