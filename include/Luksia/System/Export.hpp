#pragma once

#include <Luksia/Config.hpp>

#if defined(LUKSIA_SYSTEM_EXPORTS)
#define LUKSIA_SYSTEM_API LUKSIA_API_EXPORT
#else
#define LUKSIA_SYSTEM_API LUKSIA_API_IMPORT
#endif
