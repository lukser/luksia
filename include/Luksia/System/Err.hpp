#pragma once

#include <Luksia/System/Export.hpp>
#include <ostream>

namespace lk {
    LUKSIA_SYSTEM_API std::ostream &err();
}
