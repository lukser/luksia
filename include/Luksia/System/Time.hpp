#pragma once

#include <Luksia/System/Export.hpp>
#include <cstdint>
#include <chrono>

namespace lk {
    class Time {
    public:
        constexpr Time();

        template<typename Rep, typename Period>
        explicit constexpr Time(const std::chrono::duration<Rep, Period> &duration);

        constexpr float asSeconds() const;

        constexpr std::int32_t asMilliseconds() const;

        constexpr std::int64_t asMicroseconds() const;

        constexpr std::chrono::microseconds toDuration() const;

        template<typename Rep, typename Period>
        explicit constexpr operator std::chrono::duration<Rep, Period>() const;

        static const Time Zero;

        constexpr auto operator<=>(const Time &) const = default;

    private:
        friend constexpr Time seconds(float);

        friend constexpr Time milliseconds(std::int32_t);

        friend constexpr Time microseconds(std::int64_t);

        std::chrono::microseconds m_microseconds{};
    };

    constexpr Time seconds(float amount);

    constexpr Time milliseconds(std::int32_t amount);

    constexpr Time microseconds(std::int64_t amount);


    constexpr Time operator-(Time right);

    constexpr Time operator-(Time left, Time right);

    constexpr Time operator-=(Time &left, Time right);

    constexpr Time operator+(Time left, Time right);

    constexpr Time operator+=(Time &left, Time right);

    constexpr Time operator*(Time left, float right);

    constexpr Time operator*(Time left, std::int64_t right);

    constexpr Time operator*(float left, Time right);

    constexpr Time operator*(std::int64_t left, Time right);

    constexpr Time &operator*=(Time &left, float right);

    constexpr Time &operator*=(Time &left, std::int64_t right);

    constexpr float operator/(Time left, Time right);

    constexpr Time operator/(Time left, float right);

    constexpr Time operator/(Time left, std::int64_t right);

    constexpr Time &operator/=(Time &first, float right);

    constexpr Time &operator/=(Time &first, std::int64_t right);

    constexpr Time operator%(Time left, Time right);

    constexpr Time &operator%=(Time &left, Time right);
}
