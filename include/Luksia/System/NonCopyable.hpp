#pragma once

#include <Luksia/System/Export.hpp>

namespace lk {
    class LUKSIA_SYSTEM_API NonCopyable {
    public:
        NonCopyable &operator=(const NonCopyable &) = delete;

        NonCopyable(const NonCopyable &) = delete;

    protected:
        NonCopyable() = default;

        ~NonCopyable() = default;
    };
}
