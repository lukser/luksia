#pragma once

// Versions
constexpr int LUKSIA_VERSION_MAJOR = 0;
constexpr int LUKSIA_VERSION_MINOR = 0;
constexpr int LUKSIA_VERSION_PATCH = 3;

// OS identity
#if defined (_WIN32)
#define LUKSIA_SYSTEM_WINDOWS

#elif defined(__APPLE__) && defined(__MACH__)
#include "TargetConditionals.h"

#if TARGET_OS_IPHONE || TRAGET_IPHONE_SIMULATOR
#define LUKSIA_SYSTEM_IOS

#elif TARGET_OS_MAC
# define LUKSIA_SYSTEM_MACOS
#else
#error Unsupported Apple operating system
#endif

#elif defined(__unix__)
#if defined(__ANDROID__)
#define LUKSIA_SYSTEM_ANDROID
#elif defined(__linux__)
#define LUKSIA_SYSTEM_LINUX
#elif defined(__FreeBSD__) || defined(__FreeBSD_kernel__)
#define LUKSIA_SYSTEM_FREEBSD
#elif defined(__OpenBSD__)
#define LUKSIA_SYSTEM_OPENBSD
#else
#error Unsupported UNIX operating system
#endif

#else
#error Unsupported operating system
#endif

// Dynamic export macros
#if !defined(LUKSIA_STATIC)

#if defined(LUKSIA_SYSTEM_WINDOWS)
#define LUKSIA_API_EXPORT __declspec(dllexport)
#define LUKSIA_API_IMPORT __declspec(dllimport)

#if defined(_MSC_VER)
#pragma warning(disable: 4251)
#endif
#else
#if __GNUC__ >= 4
#define LUKSIA_API_EXPORT __attribute__ ((__visibility ("default")))
#define LUKSIA_API_IMPORT __attribute__ ((__visibility ("default")))
#else
#define LUKSIA_API_EXPORT
#define LUKSIA_API_IMPORT
#endif
#endif
#else
#define LUKSIA_API_EXPORT
#define LUKSIA_API_IMPORT
#endif

// Deprecation
#if defined(LUKSIA_NO_DEPRECATED_WARNINGS)
#define LUKSIA_DEPRECATED
#elif defined(_MSC_VER)
#define LUKSIA_DEPRECATED __declspec(deprecated)
#elif defined(__GNUC__)
#define LUKSIA_DEPRECATED __attribute__ ((deprecated))
#else
#pragma message("LUKSIA_DEPRECATED is not supported for your compiler")
#define LUKSIA_DEPRECATED
#endif
