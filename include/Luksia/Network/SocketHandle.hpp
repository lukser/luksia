#pragma once

#include <Luksia/Config.hpp>

#if defined(LUKSIA_SYSTEM_WINDOWS)

#include <basetsd.h>

#endif

namespace lk {
#if defined(LUKSIA_SYSTEM_WINDOWS)
    using SocketHandle = UINT_PTR;
#else
    using SocketHandle = int;
#endif
}
