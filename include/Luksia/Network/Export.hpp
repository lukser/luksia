#pragma once

#include <Luksia/Config.hpp>

#if defined(LUKSIA_NETWORK_EXPORTS)
#define LUKSIA_NETWORK_API LUKSIA_API_EXPORT
#else
#define LUKSIA_NETWORK_API LUKSIA_API_IMPORT
#endif
