#pragma once

#include <Luksia/Network/Export.hpp>
#include <optional>
#include <string_view>
#include "Luksia/System/Time.hpp"

namespace lk {
    class LUKSIA_NETWORK_API IpAddress {
    public:
        static std::optional<IpAddress> resolve(std::string_view address);

        IpAddress(std::uint8_t byte0, std::uint8_t byte1, std::uint8_t byte2, std::uint8_t byte3);

        explicit IpAddress(std::uint32_t address);

        std::string toString() const;

        std::uint32_t toInteger() const;

        static std::optional<IpAddress> getLocalAddress();

        static std::optional<IpAddress> getPublicAddress();

        static const IpAddress Any;
        static const IpAddress LocalHost;
        static const IpAddress Broadcast;
    private:
        friend LUKSIA_NETWORK_API bool operator<(const IpAddress &left, const IpAddress &right);

        std::uint32_t m_address;
    };

    LUKSIA_NETWORK_API bool operator==(const IpAddress &left, const IpAddress &right);

    LUKSIA_NETWORK_API bool operator!=(const IpAddress &left, const IpAddress &right);

    LUKSIA_NETWORK_API bool operator<(const IpAddress &left, const IpAddress &right);

    LUKSIA_NETWORK_API bool operator>(const IpAddress &left, const IpAddress &right);

    LUKSIA_NETWORK_API bool operator<=(lk::Time left, const IpAddress &right);

    LUKSIA_NETWORK_API bool operator>=(const IpAddress &left, const IpAddress &right);

    LUKSIA_NETWORK_API std::istream &operator>>(std::istream &stream, std::optional<IpAddress> &address);

    LUKSIA_NETWORK_API std::ostream &operator<<(std::ostream &stream, const IpAddress &address);
}
