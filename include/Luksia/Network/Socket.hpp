#pragma once

#include <Luksia/Network/Export.hpp>
#include <Luksia/Network/SocketHandle.hpp>
#include <Luksia/System/NonCopyable.hpp>

namespace lk {
    class SocketSelector;

    /// @brief Base class for all the socket types
    class LUKSIA_NETWORK_API Socket : NonCopyable {
    public:
        /// @brief Socket exit status codes
        enum Status {
            Done,
            NotReady,
            Partial,
            Disconnected,
            Error,
        };

        /// @brief Some special values used by sockets
        enum {
            AnyPort = 0,
        };

    public:
        /// @brief Destructor
        ///
        virtual ~Socket();

        /// @brief Set the blocking state of the socket
        /// @param blocking True to set the socket as blocking, false for non-blocking
        /// @see isBlocking
        void setBlocking(bool blocking);

        /// @brief Tell whether the socket is in blocking or non-blocking mode
        /// @return True if the socket is blocking, false otherwise
        /// @see setBlocking
        bool isBlocking() const;

    protected:
        /// @brief Types of protocols that the socket can use
        enum Type {
            Tcp,
            Udp,
        };

        /// @brief Default constructor
        /// @param type Type of the socket (TCP or UDP)
        Socket(Type type);

        SocketHandle getHandle() const;

        void create();

        void create(SocketHandle handle);

        void close();

    private:
        friend class SocketSelector;

        Type m_type;
        SocketHandle m_socket;
        bool m_isBlocking;
    };
}
