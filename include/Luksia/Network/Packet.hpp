#pragma once

#include <Luksia/Network/Export.hpp>
#include <cstddef>
#include <cstdint>
#include <vector>
#include <string>

namespace lk {
    class TcpSocket;

    class UdpSocket;

    class LUKSIA_NETWORK_API Packet {
    public:
        Packet();

        virtual ~Packet();

        Packet(const Packet &other);

        Packet &operator=(const Packet &other);

        Packet(Packet &&other) noexcept;

        Packet &operator=(Packet &&other) noexcept;

        void append(const void *data, std::size_t sizeInBytes);

        std::size_t getReadPosition() const;

        void clear();

        const void *getData() const;

        std::size_t getDataSize() const;

        bool endOfPacket() const;

        explicit operator bool() const;

        Packet &operator>>(bool &data);

        Packet &operator>>(std::int8_t &data);

        Packet &operator>>(std::uint8_t &data);

        Packet &operator>>(std::int16_t &data);

        Packet &operator>>(std::uint16_t &data);

        Packet &operator>>(std::int32_t &data);

        Packet &operator>>(std::uint32_t &data);

        Packet &operator>>(std::int64_t &data);

        Packet &operator>>(std::uint64_t &data);

        Packet &operator>>(float &data);

        Packet &operator>>(double &data);

        Packet &operator>>(char *data);

        Packet &operator>>(std::string &data);

        Packet &operator>>(wchar_t *data);

        Packet &operator>>(std::wstring &data);

        Packet &operator<<(bool data);

        Packet &operator<<(std::int8_t data);

        Packet &operator<<(std::uint8_t data);

        Packet &operator<<(std::int16_t data);

        Packet &operator<<(std::uint16_t data);

        Packet &operator<<(std::int32_t data);

        Packet &operator<<(std::uint32_t data);

        Packet &operator<<(std::int64_t data);

        Packet &operator<<(std::uint64_t data);

        Packet &operator<<(float data);

        Packet &operator<<(double data);

        Packet &operator<<(const char *data);

        Packet &operator<<(const std::string &data);

        Packet &operator<<(const wchar_t *data);

        Packet &operator<<(const std::wstring &data);

    protected:
        friend class TcpSocket;

        friend class UdpSocket;

        virtual const void *onSend(std::size_t &size);

        virtual void onReceive(const void *data, std::size_t size);

    private:
        bool operator==(const Packet &right) const;

        bool operator!=(const Packet &right) const;

        bool checkSize(std::size_t size);

        std::vector<char> m_data;
        std::size_t m_readPosition{};
        std::size_t m_sendPosition{};
        bool m_isValid{true};
    };
}
