#pragma once

#include <Luksia/Network/Export.hpp>
#include <Luksia/Network/Socket.hpp>
#include <Luksia/Network/IpAddress.hpp>

namespace lk {
    class TcpSocket;

    class LUKSIA_NETWORK_API TcpListener : public Socket {
    public:
        TcpListener();

        std::uint16_t getLocalPort() const;

        Status listen(std::uint16_t port, const IpAddress &address = IpAddress::Any);

        void close();

        Status accept(TcpSocket &socket);
    };
}
