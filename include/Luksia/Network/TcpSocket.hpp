#pragma once

#include <Luksia/Network/Export.hpp>
#include <Luksia/Network/Socket.hpp>
#include <Luksia/System/Time.hpp>
#include <optional>
#include <vector>

namespace lk {
    class TcpListener;

    class IpAddress;

    class Packet;

    class LUKSIA_NETWORK_API TcpSocket : public Socket {
    public:
        TcpSocket();

        std::uint16_t getLocalPort() const;

        std::optional<IpAddress> getRemoteAddress() const;

        std::uint16_t getRemotePort() const;

        Status connect(const IpAddress &remoteAddress, std::uint16_t remotePort, Time timeout = Time::Zero);

        void disconnect();

        Status send(const void *data, std::size_t size);

        Status send(const void *data, std::size_t size, std::size_t &sent);

        Status send(Packet &packet);

        Status receive(void *data, std::size_t size, std::size_t &received);

        Status receive(Packet &packet);

    private:
        friend class TcpListener;

        struct PendingPacket {
            std::uint32_t size{};
            std::size_t sizeReceived{};
            std::vector<char> data;
        };

        PendingPacket m_pendingPacket;
        std::vector<char> m_blockToSendBuffer;
    };
}
